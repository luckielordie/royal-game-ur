package main

import (
	"fmt"
)

type Piece struct {
	HasCompletedCircuit bool
}

type Tile struct {
	AwardsSecondRoll bool
	CurrentPiece     *Piece
	NextTile         [2]*Tile
}

func (tile *Tile) IsTileOccupied() bool {
	if tile.CurrentPiece != nil {
		return true
	}

	return false
}

type PlayerEnum int

func (player PlayerEnum) IsValid() bool {
	if player > 1 || player < 0 {
		return true
	}

	return false
}

const (
	PlayerOne PlayerEnum = 0
	PlayerTwo PlayerEnum = 1
)

type Board struct {
	StartTile map[PlayerEnum]*Tile
	Pieces    map[PlayerEnum][]Piece
	tiles     [20]Tile
	pieces    [14]Piece
}

func createPieces() [14]Piece {
	const numPieces = 14
	var pieces [14]Piece

	for i := range pieces {
		pieces[i] = Piece{false}
	}

	return pieces
}

func createTiles() [20]Tile {
	var tiles [20]Tile
	playerOneStart := tiles[0:4]
	playerTwoStart := tiles[4:8]
	sharedRun := tiles[8:16]
	playerOneEnd := tiles[16:18]
	playerTwoEnd := tiles[18:20]

	configureStartRun := func(tiles []Tile, isPlayerOne bool) {
		playerNumber := 0
		if false == isPlayerOne {
			playerNumber = 1
		}

		tiles[3].NextTile[playerNumber] = &sharedRun[0]
		tiles[3].AwardsSecondRoll = true

		tiles[2].NextTile[playerNumber] = &tiles[3]
		tiles[1].NextTile[playerNumber] = &tiles[2]
		tiles[0].NextTile[playerNumber] = &tiles[1]
	}

	configureSharedRun := func(tiles []Tile) {
		for i := range tiles {
			if i == 3 {
				tiles[i].AwardsSecondRoll = true
			}

			if i == 7 {
				tiles[i].NextTile[0] = &playerOneEnd[0]
				tiles[i].NextTile[1] = &playerTwoEnd[0]
				return
			}

			tiles[i].NextTile[0] = &tiles[i+1]
			tiles[i].NextTile[1] = &tiles[i+1]
		}
	}

	configureEndRun := func(tiles []Tile, isPlayerOne bool) {
		playerNumber := 0
		if false == isPlayerOne {
			playerNumber = 1
		}

		tiles[0].AwardsSecondRoll = true
		tiles[0].NextTile[playerNumber] = &tiles[1]
	}

	configureStartRun(playerOneStart, true)
	configureStartRun(playerTwoStart, false)
	configureSharedRun(sharedRun)
	configureEndRun(playerOneEnd, true)
	configureEndRun(playerTwoEnd, false)

	return tiles
}

func CreateBoard() Board {
	var board = Board{}
	board.tiles = createTiles()
	board.pieces = createPieces()
	board.StartTile[PlayerOne] = &board.tiles[0]
	board.StartTile[PlayerTwo] = &board.tiles[4]
	board.Pieces[PlayerOne] = board.pieces[0:7]
	board.Pieces[PlayerTwo] = board.pieces[7:14]
	return board
}

type Game struct {
	Board *Board
}

func main() {
	board := CreateBoard()

	fmt.Printf("%v", board)
}
